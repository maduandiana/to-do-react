import axios from 'axios'
import {GET_ERRORS, GET_LISTS} from './types'

export const getList = (setShowLoader) => dispatch => {
    axios.get('http://localhost:3000/lists').then(
      response => {
        setShowLoader(false)
        return dispatch({
            type: GET_LISTS,
            payload: response.data
        })
      }
    ).catch(err => {
        return dispatch({
            type: GET_ERRORS,
            payload: err.response
        })
    })
  }

  export const addToList = (data,setShowLoader) => dispatch => {
    axios.post('http://localhost:3000/lists',data).then(
        dispatch(getList(setShowLoader))
    ).catch(err => {
        return dispatch({
            type: GET_ERRORS,
            payload: err.response
        })
    })
  }

  export const deleteItem = (id,setShowLoader) => dispatch => {
    axios.delete('http://localhost:3000/lists/'+id).then(
      response => {
        dispatch(getList(setShowLoader))
      }
    ).catch(err => {
        return dispatch({
            type: GET_ERRORS,
            payload: err.response
        })
    })
  }
