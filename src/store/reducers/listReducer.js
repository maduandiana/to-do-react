import {GET_LISTS} from '../actions/types'

const inialState = {
    lists: []
}

// eslint-disable-next-line import/no-anonymous-default-export
export default function(state=inialState, action){
    switch(action.type){
        case GET_LISTS:
            return {
                ...state,
                lists: action.payload
            }
        default:
            return state
    }
}