import {Link} from 'react-router-dom'
function Header(){
    return(
        <section className='header'>
            <div>
                LOGOTIP
            </div>
            <div>
                {/* Link берется из библиотеки react-router-dom, используется для перехода между роутами */}
                <Link to='/'>Home</Link>
                <Link to='todo'>Todo app</Link>
                <Link to='game'>Dino game</Link>
            </div>

        </section>
    )
}

export default Header