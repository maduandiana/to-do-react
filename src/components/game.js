import {useEffect} from 'react'
import Header from './header'
function Game(){
    let imgContent, userAnswer, answers,resultText,textColor
    let correct = 0
    let index = 0
    let arr = []
    let color = ['green','brown','red','orange']
    let correctAnswer = []
    useEffect(()=>{
         // eslint-disable-next-line
        imgContent = document.querySelector('.img-row')
         // eslint-disable-next-line
        answers = document.querySelectorAll('.answer')
         // eslint-disable-next-line
        resultText= document.querySelector('.resultText')
         // eslint-disable-next-line
        textColor = document.querySelector('.colorDino')
        imgContent.innerHTML = ''
        textColor.textContent = color[index]
        textColor.style.color = color[index]
        createImg(8)
        generateQuestion()
    },[])
  
    // сбрасивает цвета для кнопок
    function clearAnswerColor(){
        for(let i = 0;i<answers.length; i++){
            answers[i].style.background = 'steelblue'
        }
    }
    const checkAnswer =()=>{
        console.log(userAnswer,correct)
        if(+userAnswer===correct){
            resultText.textContent = 'YES, YOU ARE RIGHT'
            resultText.style.color = 'green'
            index++
            if(index > color.length-1){
                index = 0
            }
            textColor.textContent = color[index]
            textColor.style.color = color[index]
            clearAnswerColor()
            imgContent.innerHTML = ''
            arr = []
            createImg(8)
            generateQuestion()
        }else{
            resultText.textContent = 'NO, CHOOSE RIGHT ANSWER'
            resultText.style.color = 'red'
            clearAnswerColor()
        }
    }
    
    const newPic = () => {
        let image = document.createElement('img');
        let randomNum = Math.floor(Math.random()*4)+1;
        image.src=`/dinos/image${randomNum}.png`; 
        image.style.width = "100px";
        imgContent.appendChild(image);
        arr.push(randomNum);
    }
    
    // создаем по указонному количеству картинки
    const createImg = (num) => {
        for(let i=0;i<num;i++){
            newPic()
        }
    }
    function generateQuestion(){
        // определяем правильный ответ
        if(color[index]==='green'){
            correct = arr.filter(item => item === 2).length
        }else if(color[index]==='red'){
            correct = arr.filter(item => item === 4).length
        }else if(color[index]==='brown'){
            correct = arr.filter(item => item === 1).length
        }else{
            correct = arr.filter(item => item === 3).length
        }
        
        // создаем массив из вариантов ответа
        correctAnswer = [correct,correct+1,correct+3,correct+2]
        // debugger;
        // перемещаем элементы массива
        randomArrayShuffle(correctAnswer)
        // обрабатываем кнопки для варианты ответок
        for (let i=0;i<4;i++){
            answers[i].innerHTML = correctAnswer[i]
             // eslint-disable-next-line
            answers[i].addEventListener('click',()=>{
                userAnswer = answers[i].innerHTML
                 // eslint-disable-next-line
                clearAnswerColor()
                 // eslint-disable-next-line
                answers[i].style.background = 'green'
            })  
        }
    }
    function randomArrayShuffle(array) {
        var currentIndex = array.length
        var temporaryValue
        var randomIndex
        while (0 !== currentIndex) {
          randomIndex = Math.floor(Math.random() * currentIndex);
          currentIndex -= 1;
          temporaryValue = array[currentIndex];
          array[currentIndex] = array[randomIndex];
          array[randomIndex] = temporaryValue;
        }
        return array;
    }
    return(
    <section className="main">
        <Header/>
        <div className="question-block">
            <h1>How many <span className="colorDino"></span> dinos?</h1>
        </div>
        <div className="img-row">
        </div>
        <h2 className="resultText">-</h2>
        <div className="answer-btns">
            <button className="answer">3</button>
            <button className="answer">1</button>
            <button className="answer">2</button>
            <button className="answer">4</button>
        </div>
        <button className="submit" onClick={()=>checkAnswer()}>SUBMIT</button>
    </section>
    )
}

export default Game