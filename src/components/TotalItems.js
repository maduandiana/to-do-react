import {deleteItem} from '../store/actions/listActions'
import {connect} from 'react-redux'
function TotalItem(props){
  const {lists} = props.listReducer
  const deleteAll = () =>{
    props.setShowLoader(true)
    lists.map(item => (
      props.deleteItem(item.id,props.setShowLoader)
    ))
  }
    return (
        <div className='clearDiv'>
          <p>You have {lists.length} pending tasks</p>
          <button className='clearBtn' onClick={() => deleteAll()}>Clear All</button>
        </div>
    )
}

const mapStateToProps=(state)=>({
  listReducer: state.listReducer
})

export default connect(mapStateToProps, {deleteItem})(TotalItem)
