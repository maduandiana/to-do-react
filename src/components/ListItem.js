import {deleteItem} from '../store/actions/listActions'
import {connect} from 'react-redux'
function ListItem(props){
    const deleteTask = () => {
        props.setShowLoader(true)
        props.deleteItem(props.itemData.id,props.setShowLoader)
    }
    return (
        <li key={props.itemData.id}>{props.itemData.text}<button className='deleteBtn' onClick={() => deleteTask()}>🗑️</button></li>
    )
}

const mapStateToProps=(state)=>({
    listReducer: state.listReducer
})
  
export default connect(mapStateToProps, {deleteItem})(ListItem)
  