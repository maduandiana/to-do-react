import axios from 'axios'
import {useState} from 'react'
import Header from './header'
import { YMaps, Map } from 'react-yandex-maps';

function MainPage(){
    const [cityName,setCityName] = useState('')
    const [list,setList] = useState([])
    const [coords,setCoords] = useState({})
    const [icon,setIcon] = useState('https://demo.themezy.com/system/resources/demo_themes/000/000/128//images/icons/icon-1.svg')
    const [data,setData] = useState({})
    const getWeatherData = async () => {
        try{
            axios.get(`https://api.openweathermap.org/data/2.5/weather?q=${cityName}&appid=c05c1e37c38ab839523ad145494f7269&units=metric`).then( response =>{
                setData(response.data);
                setIconData(response.data)
            }
            ).catch(err => {
                alert('Enter correct city name!')
            })
            axios.get(`http://api.openweathermap.org/data/2.5/forecast?q=${cityName}&appid=c05c1e37c38ab839523ad145494f7269&units=metric`).then(response => {
                setList(response.data?.list)
            })
        } catch {
            alert('4o4')
        }
    }
    const getWeatherByMap = async (data) => {
        try{
            axios.get(`https://api.openweathermap.org/data/2.5/find?lat=${data[0]}&lon=${data[1]}&appid=c05c1e37c38ab839523ad145494f7269&units=metric`).then( response =>{
                setData(response.data.list[0]);
                setIconData(response.data)
            }
            ).catch(err => {
                alert('Enter correct city name!')
            })
        } catch {
            alert('4o4')
        }
    }

    const setIconData = (data) => {
        if(data && data.weather){
            if(data.weather[0].main === 'Clouds'){
                setIcon('https://demo.themezy.com/system/resources/demo_themes/000/000/128//images/icons/icon-5.svg')
            }else if(data.weather[0].main === 'Rain'){
                setIcon('https://demo.themezy.com/system/resources/demo_themes/000/000/128//images/icons/icon-13.svg')
            }else if(data.weather[0].main === 'Snow'){
                setIcon('https://demo.themezy.com/system/resources/demo_themes/000/000/128//images/icons/icon-14.svg')
            }
        }
    }
    const setIconData2 = (data) => {
        if(data && data.weather){
            if(data.weather[0].main === 'Clouds'){
                return 'https://demo.themezy.com/system/resources/demo_themes/000/000/128//images/icons/icon-5.svg'
            }else if(data.weather[0].main === 'Rain'){
                return 'https://demo.themezy.com/system/resources/demo_themes/000/000/128//images/icons/icon-13.svg'
            }else if(data.weather[0].main === 'Snow'){
                return 'https://demo.themezy.com/system/resources/demo_themes/000/000/128//images/icons/icon-14.svg'
            }else if(data.weather[0].main === 'Clear'){
                return 'https://demo.themezy.com/system/resources/demo_themes/000/000/128//images/icons/icon-1.svg'
            }
        }
    }
    const onMapClick = (e) => {
        const coords = e.get("coords");
        setCoords(coords)
        getWeatherByMap(coords)
    };
    console.log(JSON.stringify(coords,null,2))
    return(
        <section>
            <Header/>
            
            <div className='search-city'>
                <input value={cityName} onChange={(e)=>setCityName(e.target.value)} placeholder='Enter city' maxLength='100'/>
                <button onClick={()=>getWeatherData()}>Find</button>
            </div>
            <YMaps>
                <div className='map'>
                <Map width={1300} onClick={(e) => onMapClick(e)} height={400} defaultState={{ center: [55.75, 37.57], zoom: 9 }} />
                </div>
            </YMaps>
            <div className='weather-content'>
                <div className='today'>
                <div className='row'>
                    <p>Saturday</p>
                    <p>29 May</p>
                </div>
                <div className='column'>
                    <h2>{data?.name}</h2>
                    {/* <h2>{data ? data.name: ''}</h2> */}
                    <div className='row'>
                        <h1>{data?.main?.temp} °C</h1>
                        <img src={icon} className='today-icon' alt='icon'/>
                    </div>
                    <div className='row'>
                        <div className='row'>
                            <img src='https://demo.themezy.com/system/resources/demo_themes/000/000/128//images/icon-umberella@2x.png' alt='icon'/>
                            <span>{data?.main?.humidity}%</span>
                        </div>
                        <div className='row'>
                            <img src='https://demo.themezy.com/system/resources/demo_themes/000/000/128//images/icon-wind@2x.png' alt='icon'/>
                            <span>{data?.wind?.speed}km/h</span>
                        </div>
                        <div className='row'>
                            <img src='https://demo.themezy.com/system/resources/demo_themes/000/000/128//images/icon-compass@2x.png' alt='icon'/>
                            <span>East</span>
                        </div>
                    </div>
                </div>          
                </div>
                {list?.map(item => (
                    item?.dt_txt.split(' ')[1] === '21:00:00' ?
                        <div className='next'>
                            <p>{item?.dt_txt.split(' ')[0]}</p>
                            <img src={setIconData2(item)} alt='icon' />
                            <h1>{item?.main?.temp} °C</h1>
                            <p>{item?.main?.temp_min} °C</p>
                        </div>: ''
                    ))}
            </div>
        </section>
    )
}

export default MainPage