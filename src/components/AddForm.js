import {addToList} from '../store/actions/listActions'
import {connect} from 'react-redux'
function AddForm(props){
  const addItem = (e) =>{
    if(props.text !== ''){
      props.setShowLoader(true)
      props.addToList({text:props.text},props.setShowLoader)
      props.setText('')
      e.preventDefault();
    }else{
      alert('Enter task')
    }
  }
    return (
        <form className="center">
        <input className='text' type='text' placeholder='Add your new todo' value={props.text} onChange={(e) => { props.setText(e.target.value) }} />
        <button className='addBtn' onClick={(e) => addItem(e)}>➕</button>
      </form>
    )
}
const mapStateToProps=(state)=>({
  listReducer: state.listReducer
})

export default connect(mapStateToProps, {addToList})(AddForm)
