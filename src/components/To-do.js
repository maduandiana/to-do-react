import { useState, useEffect } from "react";
import ListItem from './ListItem'
import TotalItem from './TotalItems'
import AddForm from './AddForm'
import {connect} from 'react-redux'
import {getList} from '../store/actions/listActions'
import Header from './header'
function ToDo(props) {
  const [text, setText] = useState("");
  const [showLoader, setShowLoader] = useState(true);
  
  useEffect(()=>{
    props.getList(setShowLoader)
    // eslint-disable-next-line
  },[])
  const {lists} = props.listReducer
  return (
    <div className="App">
      <Header/>
      <div className='todos'>
        <h1 className='title'>Todo App</h1>
       <AddForm text={text} setText={setText} setShowLoader={setShowLoader}/>
       {showLoader ? 
       <div class="loader"></div>:
        <ul>
          {
            lists.map((item) => (
              <ListItem itemData={item} key={item.id} setShowLoader={setShowLoader}/>
            )) 
          } 
        </ul>
        }
        <TotalItem setShowLoader={setShowLoader}/>
      </div>
    </div>
  );
}

const mapStateToProps=(state)=>({
    listReducer: state.listReducer
})

export default connect(mapStateToProps, {getList})(ToDo)
