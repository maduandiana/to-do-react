import "./App.css";
import store from './store/store'
import ToDo from './components/To-do'
import Game from './components/game'
import {Provider} from 'react-redux'
import MainPage from './components/mainPage'
import {Route, BrowserRouter} from 'react-router-dom';
function App() {
  return (
    <Provider store={store}>
       <BrowserRouter>
        <Route exact path='/todo' component={ToDo}/>
        <Route exact path='/game' component={Game}/>
        <Route exact path='/' component={MainPage}/>
       </BrowserRouter>
    </Provider>
  );
}

export default App;
